#!/usr/bin/env python3
#-*- coding: utf-8 -*-

import fonctions as fonctions
import fonctions as otbappli 

'''
listinputs  = [{'il':'/home/ronant/Documents/Spot6/Pan_NDVI_TOA_IMG_S6P.TIF', 'out':'/home/ronant/Documents/Spot6/Pan_NDVI_TOA_IMG_S6P.xml'}]
for param1 in listinputs:
    fonctions.imageStats(param1)

listinputs = [{'in':'/home/ronant/Documents/Spot6/Pan_NDVI_TOA_IMG_S6P.TIF', 'vec':'/home/ronant/Documents/Stage_Cesbio_2020/carhab/Classifications_Vanoise/echantillons_strates_elementaires/samples_VA_thrs_5cls.gpkg', 'field':'code', 'out':'/home/ronant/Documents/Spot6/PolygonStats.xml'}]
for param2 in listinputs:
    fonctions.polygonClassStatistics(param2)
'''
listinputs = [{'in':'/home/ronant/Documents/Spot6/Pan_NDVI_TOA_IMG_S6P.TIF', 'vec':'/home/ronant/Documents/Stage_Cesbio_2020/carhab/Classifications_Vanoise/echantillons_strates_elementaires/samples_VA_thrs_5cls.gpkg', 'field':'code', 'instats':'/home/ronant/Documents/Spot6/PolygonStats.xml', 'strategy.byclass.in':'10', 'out':'/home/ronant/Documents/Spot6/SampleSelection.sqlite'}]
for param3 in listinputs:
    fonctions.sampleSelection(param3)

listinputs = [{'in':'/home/ronant/Documents/Spot6/Pan_NDVI_TOA_IMG_S6P.TIF', 'vec':'/home/ronant/Documents/Spot6/SampleSelection.sqlite', 'field':'code', 'instats':'/home/ronant/Documents/Spot6/PolygonStats.xml', 'strategy.byclass.in':'10', 'out':'/home/ronant/Documents/Spot6/SampleSelection.sqlite'}]
for param4 in listinputs:
    fonctions.sampleSelection(param4)

#une modification