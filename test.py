#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from Common import OtbAppBank as otbapp

def imageStats (param1):
    app = otbApplication.Registry.CreateApplication("otbcli_ComputeImagesStatistics")
    app.SetParameterStringList("il", ['param1'])
    app.SetParameterString("out", "EstimateImageStatistics.xml")
    
    app.ExecuteAndWriteOutput()
    images = ["/home/ronant/Documents/Spot6/Pan_NDVI_TOA_IMG_S6P.TIF"]
    for img in list(images):
        imageStats(img)
"""
def imageSAmpleSelection (param1):
    app = otbApplication.Registry.CreateApplication("otbcli_ComputeImagesStatistics")
    app.SetParameterString("in", param1)
    app.SetParameterString("vec", "variousVectors.sqlite")
    app.SetParameterString("field", "label")
    app.SetParameterString("instats", "apTvClPolygonClassStatisticsOut.xml")
    app.SetParameterString("out", "resampledVectors.sqlite")
    app.ExecuteAndWriteOutput()
images = ["/home/ronant/data/spot6_1.tif", "/home/ronant/data/spot6_2.tif"]
"""
