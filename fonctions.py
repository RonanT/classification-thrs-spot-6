#!/usr/bin/env python3
#-*- coding: utf-8 -*-

from Common import OtbAppBank as otbapp
import otbApplication as otb
'''
def imageStats(param1):
    app = otb.Registry.CreateApplication("ComputeImagesStatistics")
    app.SetParameterStringList("il", [param1["il"]])
    app.SetParameterString("out", param1["out"])
    
    app.ExecuteAndWriteOutput()

def polygonClassStatistics(param2):
    app = otb.Registry.CreateApplication("PolygonClassStatistics")
    app.SetParameterString("in", param2["in"])
    app.SetParameterString("vec", param2["vec"])
    app.SetParameterString("field", param2["field"])
    app.SetParameterString("out", param2["out"])
    
    app.ExecuteAndWriteOutput()
'''
def sampleSelection(param3):
    app = otb.Registry.CreateApplication("SampleSelection")
    app.SetParameterString("in", param3["in"])
    app.SetParameterString("vec", param3["vec"])
    app.SetParameterString("field", param3["field"])
    app.SetParameterString("instats", param3["instats"])
    app.SetParameterString("out", param3["out"])
    
    app.ExecuteAndWriteOutput()

def sampleExtraction(param4):
    app = otb.Registry.CreateApplication("SampleExtraction")
    app.SetParameterString("in", param4["in"])
    app.SetParameterString("vec",  param4["vec"]
    app.SetParameterString("outfield","prefix")
    app.SetParameterString("outfield.prefix.name", "band_")
    app.SetParameterString("field", "label")
    app.SetParameterString("out", "sample_values.sqlite")
    
    app.ExecuteAndWriteOutput()
